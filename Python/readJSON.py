#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Ernesst <slash.tux@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import datetime
import json
import traceback
from pprint import pprint
JSONFILE = "/home/phablet/.local/share/umarks.ernesst/bookmark.json"

def read_JSON():
# Read bookmak file
 with open(JSONFILE) as input:
  data_loaded = json.load(input)
 temp = []
 tagvalue = []
 for i in range (0, len (data_loaded['data'])):
  if len (data_loaded['data'][i]['tags']) == 0:
   tagvalue = ""
  else:
   tagvalue = data_loaded['data'][i]['tags'][0]
  element = {'title': data_loaded['data'][i]['title'],
  'url': data_loaded['data'][i]['url'],
  'description': data_loaded['data'][i]['description'],
  'avatar': data_loaded['data'][i]['avatar'],
  'tags': tagvalue,
  }
  temp.append(element)
 zip(temp)
 return temp
