#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Ernesst <slash.tux@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
import json
import traceback
import os, sys, inspect
from collections import OrderedDict
JSONFILE = "/home/phablet/.local/share/umarks.ernesst/bookmark.json"

# Add local modules
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
 sys.path.insert(0, cmd_folder)
cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"./lib/python3.5/site-packages/")))
if cmd_subfolder not in sys.path:
 sys.path.insert(0, cmd_subfolder)
import requests
from requests.auth import HTTPBasicAuth



def fetch_JSON(host, username, password):
 url = host
 user = username

#building the REST path to communicate with the cloud
 ocPath = url + '/index.php/apps/bookmarks/public/rest/v2/bookmark'
 ocPath += '?'
 ocPath += '&page=-1'
 r = requests.get(ocPath, auth=HTTPBasicAuth(user, password))


#create app folder in case
 directory = os.path.dirname(JSONFILE)
 if not os.path.exists(directory):
   os.makedirs(directory)

#writing down JSON
 f = open(JSONFILE, "w")

 f.write(r.text)
 f.close()

 with open(JSONFILE) as input:
   data_loaded = json.load(input)

#Adding favicons address to JSON from Duckduckgo
 for i in range (0, len (data_loaded['data'])):
    lines = data_loaded['data'][i]['url']
    lines_clean = lines.replace("http://","")
    lines_clean2 = lines_clean.replace("https://","")
    lines_clean4 = lines_clean2.split('/')[0]
    urls = 'https://icons.duckduckgo.com/ip2/'
    urls += lines_clean4
    urls += '.ico'

    data_loaded['data'][i]['avatar'] = urls

 with open(JSONFILE, 'w') as json_file:
   json.dump(data_loaded, json_file)
   json_file.close()
