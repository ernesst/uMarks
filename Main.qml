/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com>
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import Ubuntu.Components.ListItems 1.1
import io.thp.pyotherside 1.4 // This will allow us to use python. We need the library in ../lib/arm-linux-gnueabihf

MainView {
    id: root
    applicationName: "umarks.ernesst"
    automaticOrientation: true

    width: units.gu(40)
    height: units.gu(75)

    property bool initialized: false
    property var activeTransfer: null


    Timer {
        interval: 2000; running: true; repeat: false
        onTriggered: root.initialized = true
    }

    Connections {
        target: root.activeTransfer
        onStateChanged: {
            console.log("[log]: StateChanged: " + root.activeTransfer.state)
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Connections {
        target: ContentHub
        onImportRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }

        onShareRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Accounts {
        id: accounts
        onAuthenticatedChanged: if (authenticated) {
            bookmarks.login(username, password)
        }
    }

    BookmarkModel {
        id: bookmarks
        host: accounts.host
    }

    PageStack {
        id: pageStack

        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            visible: false

            header: PageHeader {
                title: i18n.tr("uMarks")
                flickable: bookmarkView.visible ? bookmarkView : null
                StyleHints {
                                        foregroundColor: "White"
                                        backgroundColor: "#225684"
                                        dividerColor: UbuntuColors.slate
                                                }
                ActivityIndicator {
                    id:refreshIndicator
                    objectName: "activityIndicator"
                    anchors.centerIn: parent
                   // ColorAnimation: UbuntuColors.orange
                }
                trailingActionBar.actions: [
                    Action {
                        iconName: "reload"
                      onTriggered: {
                          refreshIndicator.visible = true
                          refreshIndicator.running = true
                          refreshIndicator.focus = true
                          py.read_JSON();
                          bookmarks.login(accounts.username, accounts.password);
                          console.log("[LOG]: reload request");
                      }
                    },
                    Action {
                        iconName: "contact"
                        onTriggered: accounts.choose()
                    }

                ]
            }


            ActivityIndicator {
                anchors.centerIn: parent
                visible: !initialized
                running: !initialized
            }

            BookmarkView {
                id: bookmarkView
                anchors.fill: parent
                model: bookmarks
                accounts: accounts
                visible: initialized && bookmarks.count > 0
                //visible: bookmarks.count > 0
            }

            ImportPrompt {
                id: importPrompt
                visible: initialized && bookmarks.count === 0
                onImportRequested: pageStack.push(Qt.resolvedUrl("ImportPage.qml"))
            }
        }
    }

    function shareItems(items) {
        for (var i = 0; i < items.length; i++) {
            var url = items[i].url
            console.log("Adding URL: " + url)
            // uploadModel.addPhoto(url)
        }
    }

//Python funtion definition
    Python {
                    id: py
                     Component.onCompleted: {
                         addImportPath(Qt.resolvedUrl("./"));
                         addImportPath(Qt.resolvedUrl("./Python"));
                         importModule("readJSON", function() {});
                         importModule("fetchJSON", function() {});
                         importModule("sortJSON", function() {});
                         py.read_JSON();
                         }

                     function sort_JSON() {py.call("sortJSON.sort_JSON", [], function() {});}
                     function fetch_JSON() {py.call("fetchJSON.fetch_JSON", [accounts.host, accounts.username, accounts.password], function() {});}
                     function read_JSON() {py.call("readJSON.read_JSON", [], function(result) {
                                            console.log("[LOG]: Reading contents from Python");
                                            bookmarks.clear();
                                            bookmarks.clear();
                                            for (var i=0; i<result.length; i++)
                                               {bookmarks.append(result[i]);
                                                //console.log("==========================");
                                                //console.log(bookmarks.get(i).title);
                                                //console.log(bookmarks.get(i).tags);
                                                //console.log(result[i]['tags']);
                                                //console.log("==========================");
                                            }
                                            refreshIndicator.visible = false
                                            refreshIndicator.running = false
                                            refreshIndicator.focus = false
                     }
                                            );

                     }
      }


}
