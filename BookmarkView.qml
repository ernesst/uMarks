/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com> 
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3

ListView {
    id: root

    property var accounts

    spacing: 1

    delegate: BookmarkDelegate {
        anchors { left: parent.left; right: parent.right; rightMargin:units.gu(1);leftMargin:units.gu(7)   }
        height: units.gu(7)
        title: model.title
        url: model.url
        description: model.description
        tags: model.tags

    onClicked: { // to enable URL redirection
                 ListView.view.currentIndex = index
                 Qt.openUrlExternally(model.url)
    }
    }
}
