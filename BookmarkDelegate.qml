/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com> 
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root

    property var url
    property var avatar
    property string title: ""
    property string description: ""
    property string tags: ""
    //property alias tags: metadata.tags
    property var uploadStatus: 0
    property int progress: -1

    signal clicked

    states: [
        State {
            name: "ready"
            when: uploadStatus == 0
        },
        State {
            name: "uploading"
            when: uploadStatus == 1
            PropertyChanges { target: mouseArea; enabled: false }
            PropertyChanges { target: metadata; opacity: 0.3 }
            PropertyChanges { target: progressBar; opacity: 1.0 }
            PropertyChanges { target: progressText; opacity: 1.0 }
        },
        State {
            name: "uploaded"
            extend: "uploading"
            when: uploadStatus == 2
            PropertyChanges { target: progressText; opacity: 0.0 }
            PropertyChanges { target: progressText; opacity: 0.0 }
        },
        State {
            name: "failed"
            extend: "ready"
            when: uploadStatus == 3
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation {
                duration: 500
                easing.type: Easing.InOutQuad
                property: "opacity"
            }
        }
    ]

    BookmarkInfo {
        id: metadata
        anchors.fill: parent
        title: root.title
        description: root.description
        //To display url please define url
        //url:root.url
        tags: root.tags
    }

    Rectangle {
        id: progressBar
        anchors { left: metadata.left; bottom: parent.bottom }
        opacity: 0.0
        width: metadata.width * progress / 100
        height: units.gu(0.5)
        color: "#ff0084"
    }

    Label {
        id: progressText
        anchors.centerIn: metadata
        opacity: 0.0
        text: i18n.tr("%1%").arg(progress)
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
