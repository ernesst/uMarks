/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com> 
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import Ubuntu.OnlineAccounts 2.0

Item {
    id: root

    property string accountName: accountConnection.target ? accountConnection.target.displayName : ""
    property bool authenticated: false
    property string host: ""
    property string username: ""
    property string password: ""

    function choose() {
        PopupUtils.open(dialogComponent)
    }

    function useAccount(account) {
        host = account.settings.host
        accountConnection.target = account
        account.authenticate({})
    }

    Timer {
        interval: 100
        running: true
        onTriggered: if (accounts.count === 0) {
            choose()
        } else {
            useAccount(accounts.get(0, "account"))
        }
    }

    AccountModel {
        id: accounts
        applicationId: "umarks.ernesst_umarks"
    }

    Connections {
        id: accountConnection
        target: null
        onAuthenticationReply: {
            var reply = authenticationData
            if ("errorCode" in reply) {
                console.warn("Authentication error: " + reply.errorText + " (" + reply.errorCode + ")")
                root.authenticated = false
            } else {
                root.username = reply.Username
                root.password = reply.Password
                root.authenticated = true
            }
        }

    }

    Component {
        id: dialogComponent
        Dialog {
            id: dialog
            title: i18n.tr("Choose a Nexcloud Account")

            Repeater {
                model: accounts
                ListItem.Standard {
                    anchors { left: parent.left; right: parent.right }
                    height: units.gu(6)
                    text: model.displayName
                    onClicked: {
                        useAccount(model.account)
                        PopupUtils.close(dialog)
                    }
                }
            }

            Label {
                anchors {
                    left: parent.left; right: parent.right
                    margins: units.gu(1)
                }
                visible: accounts.count === 0
                text: i18n.tr("No Nextcloud accounts available. Tap on the button below to add an account.")
                wrapMode: Text.Wrap
            }

            Button {
                text: i18n.tr("Add a new account")
                onClicked: accounts.requestAccess(accounts.applicationId + "_nextcloud", {})
            }

            Button {
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}

