/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com> 
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import Ubuntu.Content 1.3

ContentPeerPicker {
    id: root

    handler: ContentHandler.Destination
    contentType: ContentType.Links

    onPeerSelected: {
        peer.selectionType = ContentTransfer.Multiple;
        peer.request();
        pageStack.pop()
    }

    onCancelPressed: pageStack.pop()
}
